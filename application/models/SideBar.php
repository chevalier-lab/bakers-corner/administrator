<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SideBar extends CI_Model
{
    public function get()
    {
        return array(
            "mainMenus" => $this->getMainMenus(),
            "innerMenus" => $this->getInnerMenus()
        );
    }

    public function getMainMenus()
    {
        return array(
            array(
                "label" => "Dashboard",
                "href" => "#dashboard",
                "icon" => "monitor"
            ),
            array(
                "label" => "Products",
                "href" => "#products",
                "icon" => "package"
            ),
            array(
                "label" => "Request Menu",
                "href" => "#request-menu",
                "icon" => "download"
            ),
            array(
                "label" => "Transactions",
                "href" => "#transactions",
                "icon" => "file-text"
            ),
            array(
                "label" => "Master Data",
                "href" => "#master-data",
                "icon" => "database"
            ),
            array(
                "label" => "Services",
                "href" => "#services",
                "icon" => "server"
            )
        );
    }

    public function getInnerMenus()
    {
        return array(
            array(
                "id" => "dashboard",
                "heading" => "Dashboard",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/income"),
                        "label" => "Income",
                    ),
                    array(
                        "link" => base_url("index.php/views/tutorial"),
                        "label" => "Tutorial",
                    )
                )
            ),
            array(
                "id" => "products",
                "heading" => "Products",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/status_product"),
                        "label" => "Status Product",
                    ),
                    array(
                        "link" => base_url("index.php/views/new_product"),
                        "label" => "New Product",
                    ),
                    array(
                        "link" => base_url("index.php/views/list_product"),
                        "label" => "List Product",
                    ),
                    array(
                        "link" => base_url("index.php/views/voucher_product"),
                        "label" => "Voucher Product",
                    ),
                    array(
                        "link" => base_url("index.php/views/feedback_product"),
                        "label" => "Feedback Product",
                    ),
                )
            ),
            array(
                "id" => "request-menu",
                "heading" => "Request Menu",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/status_request_menu"),
                        "label" => "Status Request Menu",
                    ),
                    array(
                        "link" => base_url("index.php/views/list_request_menu"),
                        "label" => "List Request Menu",
                    )
                )
            ),
            array(
                "id" => "transactions",
                "heading" => "Transactions",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/status_transaction"),
                        "label" => "Status Transaction",
                    ),
                    array(
                        "link" => base_url("index.php/views/list_transaction_product"),
                        "label" => "List Transaction Product",
                    ),
                    array(
                        "link" => base_url("index.php/views/list_transaction_request_menu"),
                        "label" => "List Transaction Request Menu",
                    ),
                )
            ),
            array(
                "id" => "master-data",
                "heading" => "Master Data",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/icons"),
                        "label" => "Icons",
                    ),
                    array(
                        "link" => base_url("index.php/views/categories"),
                        "label" => "Categories",
                    ),
                    array(
                        "link" => base_url("index.php/views/medias"),
                        "label" => "Medias",
                    ),
                    array(
                        "link" => base_url("index.php/views/users"),
                        "label" => "Users",
                    ),
                    array(
                        "link" => base_url("index.php/views/logs"),
                        "label" => "Logs",
                    ),
                    array(
                        "link" => base_url("index.php/views/services"),
                        "label" => "Services",
                    ),
                )
            ),
            
            array(
                "id" => "services",
                "heading" => "Services",
                "items" => array(
                    array(
                        "link" => base_url("index.php/views/payment_gateway"),
                        "label" => "Payment Gateway",
                    ),
                    array(
                        "link" => base_url("index.php/views/chat"),
                        "label" => "Chat",
                    ),
                    array(
                        "link" => base_url("index.php/views/notification"),
                        "label" => "Notification",
                    ),
                    array(
                        "link" => base_url("index.php/views/announcement"),
                        "label" => "Announcement",
                    ),
                    array(
                        "link" => base_url("index.php/views/promos_and_banner"),
                        "label" => "Promos And Banner",
                    ),
                    array(
                        "link" => base_url("index.php/views/cashier"),
                        "label" => "Cashier",
                    ),
                    array(
                        "link" => base_url("index.php/views/product_cart"),
                        "label" => "Product Cart",
                    ),
                    array(
                        "link" => base_url("index.php/views/request_menu_cart"),
                        "label" => "Request Menu Cart",
                    ),
                    array(
                        "link" => base_url("index.php/views/wishlist"),
                        "label" => "Wishlist",
                    ),
                )
            ),
        );
    }
}
