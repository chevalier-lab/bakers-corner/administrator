<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Views extends CI_Controller
{
    // Public Variable
    public $session;
    public $sideBar, $navBar;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("SideBar");

        // Load Helper
        // $this->session = new Session_helper();

        // Get Side Bar
        $this->sideBar = $this->SideBar->get();

        // Check Auth
        // $this->checkAuth();
    }

    // Index
    public function index()
    {
        header('Location: ' . base_url("index.php/views/income"));
    }

    // ============================================================================= Dashboard
    public function income()
    {
        $this->load->view("dashboard/income/index", array(
            'title' => "Income",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Dashboard",
                'innerMenu' => "Income"
            )
        ));
    }

    public function tutorial()
    {
        $this->load->view("dashboard/tutorial/index", array(
            'title' => "Tutorial",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Dashboard",
                'innerMenu' => "Tutorial"
            )
        ));
    }

    // ============================================================================= Product
    public function status_product()
    {
        $this->load->view("products/status-product/index", array(
            'title' => "Status Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Products",
                'innerMenu' => "Status Product"
            )
        ));
    }

    public function new_product()
    {
        $this->load->view("products/new-product/index", array(
            'title' => "New Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Products",
                'innerMenu' => "New Product"
            )
        ));
    }

    public function list_product()
    {
        $this->load->view("products/list-product/index", array(
            'title' => "List Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Products",
                'innerMenu' => "List Product"
            )
        ));
    }

    public function voucher_product()
    {
        $this->load->view("products/voucher-product/index", array(
            'title' => "Voucher Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Products",
                'innerMenu' => "Voucher Product"
            )
        ));
    }

    public function feedback_product()
    {
        $this->load->view("products/feedback-product/index", array(
            'title' => "Feedback Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Products",
                'innerMenu' => "Feedback Product"
            )
        ));
    }

    // ============================================================================= Request Menu
    public function status_request_menu()
    {
        $this->load->view("request-menu/status-request-menu/index", array(
            'title' => "Status Request Menu",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Request Menu",
                'innerMenu' => "Status Request Menu"
            )
        ));
    }

    public function list_request_menu()
    {
        $this->load->view("request-menu/list-request-menu/index", array(
            'title' => "List Request Menu",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Request Menu",
                'innerMenu' => "List Request Menu"
            )
        ));
    }

    // ============================================================================= Transactions
    public function status_transaction()
    {
        $this->load->view("transactions/status-transaction/index", array(
            'title' => "Status Transaction",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Transactions",
                'innerMenu' => "Status Transaction"
            )
        ));
    }

    public function list_transaction_product()
    {
        $this->load->view("transactions/list-transaction-product/index", array(
            'title' => "List Transaction Product",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Transactions",
                'innerMenu' => "List Transaction Product"
            )
        ));
    }

    public function list_transaction_request_menu()
    {
        $this->load->view("transactions/list-transaction-request-menu/index", array(
            'title' => "List Transaction Request Menu",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Transactions",
                'innerMenu' => "List Transaction Request Menu"
            )
        ));
    }

    // ============================================================================= Master Data
    public function icons()
    {
        $this->load->view("master-data/icons/index", array(
            'title' => "Icons",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Icons"
            )
        ));
    }

    public function categories()
    {
        $this->load->view("master-data/categories/index", array(
            'title' => "Categories",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Categories"
            )
        ));
    }

    public function Medias()
    {
        $this->load->view("master-data/Medias/index", array(
            'title' => "Medias",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Medias"
            )
        ));
    }

    public function users()
    {
        $this->load->view("master-data/users/index", array(
            'title' => "Users",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Users"
            )
        ));
    }

    public function logs()
    {
        $this->load->view("master-data/logs/index", array(
            'title' => "Logs",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Logs"
            )
        ));
    }

    public function services()
    {
        $this->load->view("master-data/services/index", array(
            'title' => "Services",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Master Data",
                'innerMenu' => "Services"
            )
        ));
    }

    // ============================================================================= Services
    public function payment_gateway()
    {
        $this->load->view("services/payment-gateway/index", array(
            'title' => "Payment Gateway",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Payment Gateway"
            )
        ));
    }

    public function chat()
    {
        $this->load->view("services/chat/index", array(
            'title' => "Chat",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Chat"
            )
        ));
    }

    public function notification()
    {
        $this->load->view("services/notification/index", array(
            'title' => "Notification",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Notification"
            )
        ));
    }

    public function announcement()
    {
        $this->load->view("services/announcement/index", array(
            'title' => "Announcement",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Announcement"
            )
        ));
    }

    public function promos_and_banner()
    {
        $this->load->view("services/promos-and-banner/index", array(
            'title' => "Promos And Banner",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Promos And Banner"
            )
        ));
    }

    public function cashier()
    {
        $this->load->view("services/cashier/index", array(
            'title' => "Cashier",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Cashier"
            )
        ));
    }

    public function product_cart()
    {
        $this->load->view("services/product-cart/index", array(
            'title' => "Product Cart",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Product Cart"
            )
        ));
    }

    public function request_menu_cart()
    {
        $this->load->view("services/request-menu-cart/index", array(
            'title' => "Request Menu Cart",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Request Menu Cart"
            )
        ));
    }

    public function wishlist()
    {
        $this->load->view("services/wishlist/index", array(
            'title' => "Wishlist",
            'sideBar' => $this->sideBar,
            'active' => array(
                'mainMenu' => "Services",
                'innerMenu' => "Wishlist"
            )
        ));
    }
}
