<div class="leftbar-tab-menu">
    <div class="main-icon-menu">
        <a href="<?= base_url() ?>" class="logo logo-metrica d-block text-center">
            <span>
                <img src="<?= base_url(); ?>/assets/vendors/Metrica-v2.1.0/images/logo-sm.png" alt="logo-small" class="logo-sm">
            </span>
        </a>
        <nav class="nav">
            <?php
                if (isset($sideBar['mainMenus'])) {
                    foreach ($sideBar['mainMenus'] as $item) {
                        ?>
                            <a href="<?= $item['href'] ?>" class="nav-link <?php if($item['label'] == $active['mainMenu']) echo "active" ?>" data-toggle="tooltip-custom" data-placement="right"  data-trigger="hover" title="" data-original-title="<?= $item['label'] ?>">
                                <i data-feather="<?= $item['icon'] ?>" class="align-self-center menu-icon icon-dual"></i>
                            </a> 
                        <?php
                    }
                }
            ?>
        </nav>
    </div>

    <div class="main-menu-inner">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="<?= base_url() ?>" class="logo">
                <span>
                    <img src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/images/logo-dark.png" alt="logo-large" class="logo-lg logo-dark">
                    <img src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/images/logo.png" alt="logo-large" class="logo-lg logo-light">
                </span>
            </a>
        </div>
        <div class="menu-body slimscroll">
            <?php
                if (isset($sideBar['innerMenus'])) {
                    foreach ($sideBar['innerMenus'] as $item) {
                        ?>
                        <div id="<?= $item['id'] ?>" class="main-icon-menu-pane">
                            <div class="title-box">
                                <h6 class="menu-title"><?= $item['heading'] ?></h6>       
                            </div>
                            <ul class="nav">
                                <?php foreach ($item['items'] as $innerMenu) : ?>
                                    <li class="nav-item"><a class="nav-link <?php if($innerMenu['label'] == $active['innerMenu']) echo "active" ?>" href="<?= $innerMenu['link'] ?>"><?= $innerMenu['label'] ?></a></li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <?php
                    }
                }
            ?>
        </div>
    </div>
</div>