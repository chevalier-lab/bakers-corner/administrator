<!doctype html>
<html class="no-js" lang="en">

<?php $this->load->view('components/head'); ?>

<body>

    <input type="hidden" id="base_url" value="<?= base_url("index.php"); ?>">

    <!-- Side Bar -->
    <?php $this->load->view('components/menu/sidebar'); ?>
    
    <!-- Nav Bar -->
    <?php $this->load->view('components/menu/navbar'); ?>

    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <div class="page-content-tab">
            <div class="container-fluid">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javascript:void(0);"><?= $active['mainMenu'] ?></a></li>
                                    <li class="breadcrumb-item active"><?= $active['innerMenu'] ?></li>
                                </ol>
                            </div>
                            <h4 class="page-title"><?php if (isset($title)) echo $title ?></h4>
                        </div>
                    </div>
                </div>

                <!-- Page Content-->
                <div class="row">
                    <div class="col-12">
                        <div class="card p-4">
                            <?php if (isset($content)) echo $content ?>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="footer text-center text-sm-left">
                &copy; 2021 Bakers Corner <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i class="mdi mdi-heart text-danger"></i> by Bakers Corner Team</span>
            </footer>
        </div>
    </div>

    <?php $this->load->view('components/foot'); ?>
</body>

</html>