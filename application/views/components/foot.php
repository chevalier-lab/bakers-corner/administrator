<!-- jQuery  -->
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/jquery-ui.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/metismenu.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/waves.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/feather.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/jquery.slimscroll.min.js"></script>
<script src="<?= base_url(); ?>assets/vendors/plugins/apexcharts/apexcharts.min.js"></script>

<!-- App js -->
<script src="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/js/app.js"></script>