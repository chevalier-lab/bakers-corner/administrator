<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Dashboard Admin | Bakers Corner" />
    <!-- Document Title -->
    <title><?= isset($title) ? $title: 'Dashboard Bakers Corner'; ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/images/favicon.ico" type="image/x-icon">

    <!-- App css -->
    <link href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/css/jquery-ui.min.css" rel="stylesheet">
    <link href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/css/metisMenu.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>assets/vendors/Metrica-v2.1.0/css/app.min.css" rel="stylesheet" type="text/css" />

    <?= isset($headers) ? $headers : ''; ?>
</head>